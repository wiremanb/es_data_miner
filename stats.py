# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 06:54:24 2019

@author: wireman
"""

import elasticsearch, time

def get_stats(es, customer, index_name, ep_count):
    f = open('{}_{}_stats.csv'.format(time.time(),customer), 'w+')
    f.write('index,size\n')
    res = es.stats(index=index_name, fields="store")
    avg = 0
    for index in res['indices']:
        val = ((res['indices'][str(index)]['total']['store']['size_in_bytes']/1024.00**2.00)/ep_count)
        # avg += (val/days)
        f.write('{},{}\n'.format(index, val))
    # f.write('{},{},{}'.format('','',avg))
    f.close()

def main():
    # key = customer, value = ep_count
    customers = {'10.194.110.21:9200':500, '10.194.190.21:9200':200}
    for c in customers:
        es = elasticsearch.Elasticsearch(hosts=c).indices
        get_stats(es, c.split(':'),"logstash-*-2019.06.*", customers[c])
  
if __name__ == "__main__": 
    main()
