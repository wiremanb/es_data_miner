import numpy as np
import matplotlib.pyplot as plt
import elasticsearch
import json
import time
import csv
import yaml
from datetime import datetime

def get_event(es, customer, query_str, description, index_name, fields):
    res = es.search(index=index_name, request_timeout=300, size=10, q="{}".format(query_str))
    ret_val = []
    for hit in res['hits']['hits']:
        tmp_data = {}
        for field in fields:
            try:
                tmp_data[field] = hit['_source'][field]
            except:
                #print('No field: ' + field)
                tmp_data[field] = '-'
        ret_val.append(tmp_data)
    return ret_val
    
def analyze(customer,event_id,weight,fields,customer_data):
    with open('{}_{}.csv'.format(customer, event_id), 'w') as f:
        customer_output = csv.DictWriter(f, customer_data[0].keys())
        customer_output.writeheader()
        with open('./look_for_these_things.tsv', 'r') as lftt:
            lines = lftt.readlines()
            for lftt_line in lines[1:]:
                lftt_event_id = lftt_line.split('\t')[0].split(',')
                lftt_fields = lftt_line.split('\t')[1].split(',')
                lftt_identifier = lftt_line.split('\t')[2]
                lftt_description = lftt_line.split('\t')[3]
                lftt_weight = lftt_line.split('\t')[4]
                lftt_alert = lftt_line.split('\t')[5].rstrip()
                if 'Y' in lftt_alert and event_id in lftt_event_id:
                    print(lftt_line)
                    for field in lftt_fields:
                       if field in fields:
                           for d in customer_data:
                               print(lftt_identifier.upper() + ": " + d[field].upper())
                               if lftt_identifier.upper() in d[field].upper():
                                   d["Alert"] = "Y"
        customer_output.writerows(customer_data)

def main():
    customers = ['10.194.110.21']
    with open('event_id_fields.tsv', 'r') as f:
        lines = f.readlines()
        for line in lines[1:]:
            query = line.split('\t')[0]
            fields = line.split('\t')[1].split(',')
            description = line.split('\t')[2]
            weight = line.split('\t')[3]
            for c in customers:
                print(c)
                es = elasticsearch.Elasticsearch(hosts=c+":9200")
                data = get_event(es, c, query, description, "logstash-wineventlog-2019.06*",fields)
                analyze(c, query, weight, fields, data)
if __name__ == "__main__": 
    main()