import elasticsearch
import time
import csv
import yaml
import os
import datetime

def get_event(es, customer, query_str, description, index_name, fields):
    ret_val = []
    try:
        res = es.search(index=index_name, request_timeout=300, size=10000, q="{}".format(query_str))
        for hit in res['hits']['hits']:
            tmp_data = {}
            for field in fields:
                try:
                    #.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
                    #from_time = (datetime.datetime.now() - datetime.timedelta(hours=48))
                    #to_time = datetime.datetime.now()
                    #timestamp = datetime.datetime.strptime(hit['_source']['@timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ')
                    #if timestamp >= from_time and timestamp <= to_time:
                    tmp_data[field] = hit['_source'][field]
                    #else:
                        #print("Not in 48h window...")
                except Exception as e:
                    #print('No field: ' + field)
                    print("problem: {}".format(e))
                    tmp_data[field] = '-'
            ret_val.append(tmp_data)
    except elasticsearch.ElasticsearchException as e:
        print("Issue searching...\n{}".format(e))
    return ret_val

def parse_sigma_rules(rule):
    with open(rule, 'r') as stream:
        try:
            parsed_config = yaml.safe_load(stream)
            print(parsed_config)
        except yaml.YAMLError as exc:
            print(exc)

def main():
    #parse_sigma_rules('rules\windows\powershell\powershell_downgrade_attack.yml')
    #'win_failed_logons.yml', 'win_account_lockout.yml', 'win_account_created.yml', 
    configs = ['network_susp_signature.yml']
    parsed_config = {}
    current_time = time.time()
    if not os.path.exists('./{}'.format(current_time)):
        os.makedirs('./{}'.format(current_time))
    for config in configs:
        print("config: {}".format(config))
        with open(config, 'r') as stream:
            try:
                parsed_config = yaml.safe_load(stream)
                print(parsed_config)
            except yaml.YAMLError as exc:
                print(exc)
        for host in parsed_config['customers']:
            if not os.path.exists('./{}/{}'.format(current_time, host.split(":")[0])):
                os.makedirs('./{}/{}'.format(current_time, host.split(":")[0]))
            print('customer: {}'.format(host))
            es = elasticsearch.Elasticsearch(hosts=host)
            data = get_event(es,host.split(":")[0], parsed_config['detection']['query'], parsed_config['description'],parsed_config['index'], parsed_config['detection']['fields'])
            print(data)
            if len(data) > 0:
                with open('./{}/{}/{}_{}.csv'.format(current_time, host.split(":")[0], host.split(":")[0], parsed_config['title']), 'w') as f:
                    customer_output = csv.DictWriter(f, data[0].keys())
                    customer_output.writeheader()
                    customer_output.writerows(data)
if __name__ == "__main__": 
    main()